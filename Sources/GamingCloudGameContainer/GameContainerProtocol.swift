//
//  GameContainerImpl.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore

public protocol GameServerContainerProtocol {
    init?(gameService: GameServiceRef, gameStartupData: Data)
    
    var playerTypes: [PlayerID: PlayerType] { get }
    var gameInfo: Data { get }
    
    func onAllHumanPlayersJoined()
    
    func onPlayerDidJoin(withPlayerID playerID: PlayerID, playerType: ConnectedPlayerType, playerInfoPayload: Data?)
    func onPlayerDidLeave(withID playerID: PlayerID, remainingHumanPlayerCount: Int, spectatorCount: Int)
    
    func onGameStateUpdate(withTime time: Timestamp32, messages: [PlayerMessage])
}

public protocol RealTimeGameServerContainerProtocol: GameServerContainerProtocol {
    func onGameStateUpdate(withTime time: Timestamp32, playerStreams: [PlayerID: StreamData]) -> BroadcastStreamData
}
