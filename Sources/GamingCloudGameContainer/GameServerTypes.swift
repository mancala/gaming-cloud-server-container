//
//  GameServerTypes.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore

public enum PlayerSessionState {
    case waitingToJoin(WaitingPlayerType)
    case joined(ConnectedPlayerType)
    case left
    
    public var isActive: Bool {
        switch self {
        case .waitingToJoin, .joined: return true
        case .left: return false
        }
    }
    
    public var isConnected: Bool {
        switch self {
        case .joined: return true
        case .waitingToJoin, .left: return false
        }
    }
        
    public var playerType: PlayerType? {
        switch self {
        case .waitingToJoin(let playerType):
            return playerType.isHuman ? .human : .bot
        case .joined(let playerType):
            return playerType.isHuman ? .human : .bot
        case .left:
            return nil
        }
    }
    
    public var gameToken: GameToken? {
        switch self {
        case .waitingToJoin(let waitingPlayerType):
            guard case .human(let gameToken) = waitingPlayerType else { return nil }
            return gameToken
        case .joined(let connectedPlayerType):
            guard case .human(let gameToken, _) = connectedPlayerType else { return nil }
            return gameToken
        case .left:
            return nil
        }
    }
    
    public var clientToken: ClientToken? {
        switch self {
        case .waitingToJoin, .left:
            return nil
        case .joined(let connectedPlayerType):
            guard case .human(_, let clientToken) = connectedPlayerType else { return nil }
            return clientToken
        }
    }
}

public enum PlayerType {
    case human
    case bot
    
    public var isHuman: Bool {
        switch self {
        case .human: return true
        case .bot: return false
        }
    }
}

public enum ConnectedPlayerType {
    case human(GameToken, ClientToken)
    case bot
    
    public var isHuman: Bool {
        switch self {
        case .human: return true
        case .bot: return false
        }
    }
    
    public var gameToken: GameToken? {
        switch self {
        case .human(let gameToken, _): return gameToken
        case .bot: return nil
        }
    }
}

public enum WaitingPlayerType {
    case human(GameToken)
    case bot
    
    public var isHuman: Bool {
        switch self {
        case .human: return true
        case .bot: return false
        }
    }
}

public enum BroadcastStreamData {
    case all(data: StreamData?)
    case specialized(playerData: StreamData?, spectatorData: StreamData?)
    case playerSpecialized(playerData: [PlayerID: StreamData?], spectatorData: StreamData?)
}

public enum BroadcastMessages {
    case all(messages: [Message])
    case specialized(playerMessages: [Message]?, spectatorMessages: [Message]?)
    case playerSpecialized(playerMessages: [PlayerID: [Message]], spectatorMessages: [Message]?)
}

public struct PlayerMessage {
    public let playerID: PlayerID
    public let message: Message
    
    public init(playerID: PlayerID, message: Message) {
        self.playerID = playerID
        self.message = message
    }
}
