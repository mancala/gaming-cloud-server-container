//
//  GameServiceRef.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore

public struct GameServiceRef: GameContainerProtocol {
    private unowned var parent: ParentGameContainer
    
    public init(container: ParentGameContainer) {
        self.parent = container
    }
    
    public var gameID: GameID {
        return parent.gameID
    }
    
    public var gameTokenForSpectator: GameToken {
        return parent.gameTokenForSpectator
    }
    
    public func gameToken(forPlayerID playerID: PlayerID) -> GameToken? {
        return parent.playerSessionState(forPlayerID: playerID).gameToken
    }
    
    public func broadcast(messages: BroadcastMessages, completion: (() -> ())? = nil) {
        parent.broadcast(messages: messages, completion: completion)
    }
    
    public func broadcast(message: Message) {
        let messages = BroadcastMessages.all(messages: [message])
        broadcast(messages: messages)
    }
    
    private func gameServerToClientDescription(_ type: MessageType) -> String {
        switch type {
        case 64: return "gameDidStart"
        case 65: return "playerDidMakeMove"
        case 66: return "playerDidTimeoutTurn"
        case 67: return "playerDidJoinGame"
        case 68: return "playerDidLeaveGame"
        case 69: return "playerDidInteractWithProp"
        case 70: return "playerDidStartTurnCountdown"
        case 71: return "playerDidEarnPointsAndCoins"
        default: return "<unknown>"
        }
    }
    
    public func send(message: Message, toPlayer playerID: PlayerID) {
        let messageDescription = gameServerToClientDescription(message.type)
        log(.debug, "GameServiceRef", ">>> sending: [\(messageDescription)], toPlayer=\(playerID)")
        
        let messages = BroadcastMessages.playerSpecialized(playerMessages: [playerID: [message]], spectatorMessages: nil)
        broadcast(messages: messages)
    }
    
    public func sendSpectators(message: Message) {
        let messages = BroadcastMessages.specialized(playerMessages: nil, spectatorMessages: [message])
        broadcast(messages: messages)
    }
    
    public func endGameSession() {
        parent.endGameSession()
    }
    
    public func scheduleTimer(withTimeInterval timeInterval: TimeInterval, repeats: Bool, callback: @escaping () -> ()) -> GameTimerProtocol {
        return parent.scheduleTimer(withTimeInterval: timeInterval, repeats: repeats, callback: callback)
    }
}
