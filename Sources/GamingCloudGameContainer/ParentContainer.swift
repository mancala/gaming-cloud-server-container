//
//  ParentGameContainer.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore

public protocol ParentGameContainer: class, GameContainerProtocol {
    var gameID: GameID { get }
    var gameTokenForSpectator: GameToken { get }

    func playerSessionState(forPlayerID playerID: PlayerID) -> PlayerSessionState
    
//    func replacePlayer(withPlayerID playerID: PlayerID, with playerType: ConnectedPlayerType, playerInfo: Data?)
    func broadcast(messages: BroadcastMessages, completion: (() -> ())?)
    func broadcast(messages: BroadcastMessages)
    func send(message: Message, toPlayer playerID: PlayerID)
    func sendSpectators(message: Message)
    func endGameSession(withReason reason: DisconnectReason)
    func endGameSession()
    func scheduleTimer(withTimeInterval timeInterval: TimeInterval, repeats: Bool, callback: @escaping () -> ()) -> GameTimerProtocol
}
