// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "GamingCloudGameContainer",
    products: [
        .library(
            name: "GamingCloudGameContainer",
            targets: ["GamingCloudGameContainer"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:mancala/gaming-core.git", from: "1.0.1"),
    ],
    targets: [
        .target(
            name: "GamingCloudGameContainer",
            dependencies: ["GamingCore"])
    ]
)
