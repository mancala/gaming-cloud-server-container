import XCTest
@testable import gaming_cloud_server_container

class gaming_cloud_server_containerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(gaming_cloud_server_container().text, "Hello, World!")
    }


    static var allTests : [(String, (gaming_cloud_server_containerTests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
        ]
    }
}
