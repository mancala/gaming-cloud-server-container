import XCTest
@testable import gaming_cloud_server_containerTests

XCTMain([
     testCase(gaming_cloud_server_containerTests.allTests),
])
